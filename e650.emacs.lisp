;; Appearance settings
(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode -1)
(setq inhibit-startup-message t)
(package-initialize) ;; req to load theme
(load-theme 'zenburn t)
(set-face-attribute 'default nil :family "Inconsolata" :height 130)
(set-face-attribute 'fringe nil :background "#2F343F" :foreground "#2F343F")
(set-face-attribute 'vertical-border nil :foreground "#2F343F") 
;;(set-face-attribute 'linum nil :background "#2F343F")
(set-face-background 'mode-line "#2F343F")
(set-face-background 'mode-line-inactive "#2F343F")
(set-face-attribute 'mode-line nil :box nil)
(set-face-attribute 'mode-line-inactive nil :box nil)
(global-hl-line-mode 1)
(column-number-mode 1)

;; Matlab mode
(add-to-list 'load-path "/home/rhys/src/matlab-emacs")
(load-library "matlab-load")
(setq matlab-functions-have-end 1)

(fset 'matlab-shell-clc
   [?\C-  ?\C-a ?\C-w ?\M-x ?e ?r ?a ?s ?e ?- ?b ?u ?f ?f ?e ?r return return])

;;keybinds
(global-set-key (kbd "C-= r") 'revert-buffer)
(global-set-key (kbd "C-c l") 'matlab-shell-clc)

;; Package repository (MELPA)
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(setq custom-safe-themes t)

;; c-mode
(setq cc-set-style "linux")
(put 'erase-buffer 'disabled nil)

;;latex mode disable subscripts
(eval-after-load "tex-mode" '(fset 'tex-font-lock-suscript 'ignore)) 

;; SLIME common lisp for emacs
(setq inferior-lisp-program "/usr/bin/sbcl")
(add-to-list 'load-path "/usr/share/emacs/site-lisp/slime/")
(require 'slime)
(slime-setup)
